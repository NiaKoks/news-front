import React from 'react';
import PropsTypes from 'prop-types';
import {Card,CardBody,CardTitle,CardText,CardImg} from 'reactstrap';
import {Link} from "react-router-dom";

const SingleNews = () => {
    return (
        <Card>
            <CardBody>
                <CardImg>{props.photo}</CardImg>
                <CardTitle>{props.title}</CardTitle>
                <CardText>{props.text}</CardText>
                <Link to={'/news'+props._id}>Read More</Link>
            </CardBody>
        </Card>
    );
};
SingleNews.propTypes ={
  photo: PropsTypes.string,
  _id: PropsTypes.string.isRequired,
  title: PropsTypes.string.isRequired,
  text: PropsTypes.string.isRequired
};

export default SingleNews;