import React from 'react';
import PropsTypes from 'prop-types';
import {Card,CardBody,CardTitle,CardText} from 'reactstrap';

const SingleNews = () => {
    return (
        <Card>
            <CardBody>
                <CardTitle>{props.author}</CardTitle>
                <CardText>{props.comment_text}</CardText>
            </CardBody>
        </Card>
    );
};
SingleNews.propTypes ={
    _id: PropsTypes.string.isRequired,
    author: PropsTypes.string.isRequired,
    comment_text: PropsTypes.string.isRequired
};

export default SingleNews;